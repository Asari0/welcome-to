let num = [
    {nb: 3, valeur: '1', src: "img/assets_img_1.png"}, //3 cartes de valeur 1
    {nb: 3, valeur: '2', src: "img/assets_img_2.png"},
    {nb: 3, valeur: '14', src: "img/assets_img_14.png"},
    {nb: 3, valeur: '15', src: "img/assets_img_15.png"},
    {nb: 4, valeur: '3', src: "img/assets_img_3.png"},
    {nb: 4, valeur: '13', src: "img/assets_img_13.png"},
    {nb: 5, valeur: '4', src: "img/assets_img_4.png"},
    {nb: 5, valeur: '12', src: "img/assets_img_12.png"},
    {nb: 6, valeur: '5', src: "img/assets_img_5.png"},
    {nb: 6, valeur: '11', src: "img/assets_img_11.png"},
    {nb: 7, valeur: '6', src: "img/assets_img_6.png"},
    {nb: 7, valeur: '10', src: "img/assets_img_10.png"},
    {nb: 8, valeur: '7', src: "img/assets_img_7.png"},
    {nb: 8, valeur: '9', src: "img/assets_img_9.png"},
    {nb: 9, valeur: '8', src: "img/assets_img_8.png"},
];

let game_num = [];

for (let i = 0; i < num.length; i++) {
    for (j = 0; j < num[i].nb; j++) {
        game_num.push(num[i]) //défini les cartes dans la console
    }
}
console.log(game_num) //affiche les cartes dans la console


let carte_action = [
    { nb: 18, cle: 'Piscine', src: 'img/piscine.png'}, //18 cartes piscine
    { nb: 9, cle: 'Geometre', src: 'img/geometre.png'},
    { nb: 9, cle: 'Paysagiste', src: 'img/paysagiste.png'},
    { nb: 18, value: 'Paysagiste', src: "img/paysagiste.png" },
    { nb: 18, value: 'Agent immobilier', src: "img/agent_immobilier.png" },
    { nb: 9, value: 'Geometre', src: "img/geometre.png" },

];

let game_action = [];

for (let i = 0; i < carte_action.length; i++){
    for (j = 0; j < carte_action[i].nb; j++) {
        game_action.push(carte_action[i]) //défini les cartes dans la console
    }
}
console.log(game_action) //affiche les cartes dans la console


let action_stack1 = [];
let action_stack2 = [];
let action_stack3 = [];

let num_stack1 = [];
let num_stack2 = [];
let num_stack3 = [];

let action_stacks = [action_stack1, action_stack2, action_stack3];
let num_stacks = [num_stack1, num_stack2, num_stack3];

function randomize (game_num) {
    for (i = game_num.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        tmp = game_num[i];
        game_num[i] = game_num[j];
        game_num[j] = tmp;
    }
}

function remplissage (game_num, num_stack1, num_stack2, num_stack3) {
    for (i = 0; i < game_num.length; i+=3) {
        num_stack1.push(game_num[i]);
        num_stack2.push(game_num[i+1]);
        num_stack3.push(game_num[i+2]);
    }
}
randomize (game_num)
remplissage (game_num, num_stack1, num_stack2, num_stack3)
randomize (game_action)
remplissage (game_action, action_stack1, action_stack2, action_stack3)

function next_turn()
{
    if(action_stack1.length != 0){
        card1_action = action_stack1[0];
        card2_action = action_stack2[0];
        card3_action = action_stack3[0];

        card1_num = num_stack1[0];
        card2_num = num_stack2[0];
        card3_num = num_stack3[0];

        for(let i = 0; i < action_stacks.length; i++)
        {
            action_stacks[i].splice(0, 1);
            num_stacks[i].splice(0, 1);
        }

        console.log("Voici vos carte action : "),
        console.log(card1_action);
        let id1 = document.getElementById("id1").src=card1_action.src;
        console.log(card2_action);
        let id2 = document.getElementById("id2").src=card2_action.src;
        console.log(card3_action);
        let id3 = document.getElementById("id3").src=card3_action.src;

        console.log("Voici vos carte numéro de maison : "),
        console.log(card1_num);
        let id4 = document.getElementById("id4").src=card1_num.src;
        console.log(card2_num);
        let id5 = document.getElementById("id5").src=card2_num.src;
        console.log(card3_num);
        let id6 = document.getElementById("id6").src=card3_num.src;
    }
    else
    {
        console.log("il n'y a plus de cartes disponibles dans la pioche");
    }
}